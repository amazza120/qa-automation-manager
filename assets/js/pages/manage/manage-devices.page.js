parasails.registerPage('manage-devices', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    //...
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function (){
    _.extend(this, window.SAILS_LOCALS);

    // on load hide success message and devices drop down list
    $(document).ready(function() {
      $("#success").hide();
      $("#ddlDevicesContainer").hide();
      $("#btnCUDDevice").prop( "disabled", true );
    });
  },
  mounted: async function() {

    // Create tab click
    $("#deviceCreate").click(function() {
      $("#ddlDevicesContainer").fadeOut();

      $("#deviceName").prop( "disabled", false );
      $("#deviceIdentifier").prop( "disabled", false );
      $("#deviceViewport").prop( "disabled", false );
      $("#deviceWidth").prop( "disabled", false );
      $("#deviceHeight").prop( "disabled", false );
      $("#deviceActive").prop("disabled", false );

      $("#deviceName").val('');
      $("#deviceIdentifier").val('');
      $("#deviceViewport").val('');
      $("#deviceWidth").val('');
      $("#deviceHeight").val('');
      $("#deviceActive").prop("checked", false);

      $("#btnCUDDevice").prop( "disabled", true );
      $("#btnCUDDevice").removeClass('btn-danger');
      $("#btnCUDDevice").addClass('btn-primary');
      $("#btnCUDDevice").text('Create');
    });

    // Update tab click
    $("#deviceUpdate").click(function() {
      $("#ddlDevicesContainer").fadeIn();

      $("#deviceName").prop( "disabled", true );
      $("#deviceIdentifier").prop( "disabled", true );
      $("#deviceViewport").prop( "disabled", true );
      $("#deviceWidth").prop( "disabled", true );
      $("#deviceHeight").prop( "disabled", true );
      $("#deviceActive").prop("disabled", true );

      $("#btnCUDDevice").prop( "disabled", true );
      $("#btnCUDDevice").removeClass('btn-danger');
      $("#btnCUDDevice").addClass('btn-primary');
      $("#btnCUDDevice").text('Update');
    });

    // Delete tab click
    $("#deviceDelete").click(function() {
      $("#ddlDevicesContainer").fadeIn();

      $("#deviceName").prop( "disabled", true );
      $("#deviceIdentifier").prop( "disabled", true );
      $("#deviceViewport").prop( "disabled", true );
      $("#deviceWidth").prop( "disabled", true );
      $("#deviceHeight").prop( "disabled", true );
      $("#deviceActive").prop("disabled", true );

      $("#btnCUDDevice").prop( "disabled", true );
      $("#btnCUDDevice").removeClass('btn-primary');
      $("#btnCUDDevice").addClass('btn-danger');
      $("#btnCUDDevice").text('Delete');
    });

    // Applications ddl select
    $("#ddlDevices a").click(function() {
      let selApp = $(this).attr("value");
      $.get('/get-device', {"identifier": selApp}).then(resp => {
        // Reason for switch: there is a possability of adding more options
        switch($("#btnCUDDevice").text()) {
          case "Update":
            $("#deviceName").prop( "disabled", false );
            $("#deviceViewport").prop( "disabled", false );
            $("#deviceWidth").prop( "disabled", false );
            $("#deviceHeight").prop( "disabled", false );
            $("#deviceActive").prop("disabled", false );
            $("#btnCUDDevice").prop( "disabled", false );
            break;
          case "Delete":
            $("#btnCUDDevice").prop( "disabled", false );
            break;
        }
        $("#deviceName").val(resp.name);
        $("#deviceIdentifier").val(resp.identifier);
        $("#deviceViewport").val(resp.viewport);
        $("#deviceWidth").val(resp.width);
        $("#deviceHeight").val(resp.height);
        $("#deviceActive").prop( "checked", resp.active );
      });
    });

    // Create / Update / Delete button click
    $("#btnCUDDevice").click(function() {
      let nameVal = $("#deviceName").val();
      let identifierVal = $("#deviceIdentifier").val();
      let viewportVal = $("#deviceViewport").val();
      let widthVal = $("#deviceWidth").val();
      let heightVal = $("#deviceHeight").val();
      let activeVal = $("#deviceActive").prop("checked");

      switch($("#btnCUDDevice").text()) {
        case "Create":
          $.post('/create-device', {
            "name": nameVal, 
            "identifier": identifierVal, 
            "viewport": viewportVal,
            "width": widthVal,
            "height": heightVal,
            "active": activeVal
          }).then(resp => {
            console.log(resp);
            $("#success").text(`Document id: ${resp._id} Created!`);
            $("#success").fadeIn(function() {
              $("#success").fadeOut(2500, function() {
                // force reload to query the new devices
                location.reload();
              });
            });
          });
          break;
        case "Update":
          $.post('/update-device', {
            "name": nameVal, 
            "identifier": identifierVal, 
            "viewport": viewportVal,
            "width": widthVal,
            "height": heightVal,
            "active": activeVal
          }).then(resp => {
            console.log(resp);
            $("#success").text(`Document id: ${resp._id} Updated!`);
            $("#success").fadeIn(function() {
              $("#success").fadeOut(2500, function() {
                // force reload to query the new devices
                location.reload();
              });
            });
          });
          break;
        case "Delete":
          $.post('/delete-device', {"identifier": identifierVal}).then(resp => {
            console.log(resp);
            $("#success").text(`Document id: ${resp._id} Deleted!`);
            $("#success").fadeIn(function() {
              $("#success").fadeOut(2500, function() {
                // force reload to query the new devices
                location.reload();
              });
            });
          });
          break;
      }
    });

    // active validation
    $("#deviceName,#deviceIdentifier,#deviceViewport,#deviceWidth,#deviceHeight").on("input propertychange", function() {
      let fieldsFilled = $("#deviceName").val().length > 0 
        && $("#deviceIdentifier").val().length > 0 
        && $("#deviceViewport").val().length > 0
        && $("#deviceWidth").val().length > 0
        && $("#deviceHeight").val().length > 0;
        
      if (fieldsFilled) {
        $("#btnCUDDevice").prop( "disabled", false );
      }
      else {
        $("#btnCUDDevice").prop( "disabled", true );
      }
    });
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    //...
  }
});
