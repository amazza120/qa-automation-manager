parasails.registerPage('manage-app-areas', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    //...
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function (){
    _.extend(this, window.SAILS_LOCALS);

    // on load hide success message and app areas drop down list
    $(document).ready(function() {
      $("#success").hide();
      $("#ddlAppAreasContainer").hide();
      $("#btnCUDAppArea").prop( "disabled", true );
    });
  },
  mounted: async function() {

    // global page variables
    let appIdGlb = '';

    // Create tab click
    $("#appAreaCreate").click(function() {
      $("#ddlAppAreasContainer").fadeOut();

      elementReset();

      $("#btnCUDAppArea").prop( "disabled", true );
      $("#btnCUDAppArea").removeClass('btn-danger');
      $("#btnCUDAppArea").addClass('btn-primary');
      $("#btnCUDAppArea").text('Create');
    });

    // Update tab click
    $("#appAreaUpdate").click(function() {
      $("#ddlAppAreasContainer").fadeOut();

      elementReset();

      $("#btnCUDAppArea").prop( "disabled", true );
      $("#btnCUDAppArea").removeClass('btn-danger');
      $("#btnCUDAppArea").addClass('btn-primary');
      $("#btnCUDAppArea").text('Update');
    });

    // Delete tab click
    $("#appAreaDelete").click(function() {
      $("#ddlAppAreasContainer").fadeOut();

      elementReset();

      $("#btnCUDAppArea").prop( "disabled", true );
      $("#btnCUDAppArea").removeClass('btn-primary');
      $("#btnCUDAppArea").addClass('btn-danger');
      $("#btnCUDAppArea").text('Delete');
    });

    $("#ddlApps a").click(function() {
      $('#ddlAppAreas').empty(); // empty element
      appIdGlb = $(this).attr("value");
      $.get('/get-app-area', {"app_identifier": appIdGlb}).then(resp => {
        // Reason for switch: there is a possability of adding more options
        switch($("#btnCUDAppArea").text()) {
          case "Create":
            $("#appAreaName").prop( "disabled", false );
            $("#appAreaIdentifier").prop( "disabled", false );
            $("#appIdentifier").val(appIdGlb);
            $("#appAreaActive").prop("disabled", false );
            break;
          case "Update":
            $("#ddlAppAreasContainer").fadeIn();
            break;
          case "Delete":
            $("#ddlAppAreasContainer").fadeIn();
            break;
        }
        resp.forEach(area => {
          $('#ddlAppAreas').append(`<a class="dropdown-item" value="${area._source.identifier}">${area._source.name}</a>`)
        });
      });
    });

    // Applications ddl select
    // User jquery 'on' format for dynamically inserted dom elements
    $("#ddlAppAreas").on('click', 'a', function() {
      let selAppArea = $(this).attr("value");
      $.get('/get-app-area', {"identifier": selAppArea, "app_identifier": appIdGlb}).then(resp => {
        // Reason for switch: there is a possability of adding more options
        switch($("#btnCUDAppArea").text()) {
          case "Update":
            $("#appAreaName").prop( "disabled", false );
            $("#appAreaActive").prop("disabled", false );
            $("#btnCUDAppArea").prop( "disabled", false );
            break;
          case "Delete":
            $("#btnCUDAppArea").prop( "disabled", false );
            break;
        }
        $("#appAreaName").val(resp.name);
        $("#appAreaIdentifier").val(resp.identifier);
        $("#appIdentifier").val(appIdGlb);
        $("#appAreaActive").prop( "checked", resp.active );
      });
    });

    // Create / Update / Delete button click
    $("#btnCUDAppArea").click(function() {
      let nameVal = $("#appAreaName").val();
      let identifierVal = $("#appAreaIdentifier").val();
      let appIdentifierVal = $("#appIdentifier").val();
      let activeVal = $("#appAreaActive").prop("checked");

      switch($("#btnCUDAppArea").text()) {
        case "Create":
          $.post('/create-app-area', {
            "name": nameVal,
            "identifier": identifierVal,
            "app_identifier": appIdentifierVal,
            "active": activeVal
          }).then(resp => {
            console.log(resp);
            $("#success").text(`Document id: ${resp._id} Created!`);
            $("#success").fadeIn(function() {
              $("#success").fadeOut(2500, function() {
                // force reload to query the new app areas
                location.reload();
              });
            });
          });
          break;
        case "Update":
          $.post('/update-app-area', {
            "name": nameVal,
            "identifier": identifierVal,
            "app_identifier": appIdentifierVal,
            "active": activeVal
          }).then(resp => {
            console.log(resp);
            $("#success").text(`Document id: ${resp._id} Updated!`);
            $("#success").fadeIn(function() {
              $("#success").fadeOut(2500, function() {
                // force reload to query the new app areas
                location.reload();
              });
            });
          });
          break;
        case "Delete":
          $.post('/delete-app-area', {"identifier": identifierVal, "app_identifier": appIdentifierVal}).then(resp => {
            console.log(resp);
            $("#success").text(`Document id: ${resp._id} Deleted!`);
            $("#success").fadeIn(function() {
              $("#success").fadeOut(2500, function() {
                // force reload to query the new app areas
                location.reload();
              });
            });
          });
          break;
      }
    });

    // active validation
    $("#appAreaName,#appAreaIdentifier,#appIdentifier").on("input propertychange", function() {
      let fieldsFilled = $("#appAreaName").val().length > 0 
        && $("#appAreaIdentifier").val().length > 0
        && $("#appIdentifier").val().length > 0;
        
      if (fieldsFilled) {
        $("#btnCUDAppArea").prop( "disabled", false );
      }
      else {
        $("#btnCUDAppArea").prop( "disabled", true );
      }
    });

    function elementReset() {
      $("#appAreaName").prop( "disabled", true );
      $("#appAreaIdentifier").prop( "disabled", true );
      $("#appAreaActive").prop("disabled", true );

      $("#appAreaName").val('');
      $("#appIdentifier").val('');
      $("#appAreaIdentifier").val('');
      $("#appAreaActive").prop("checked", false);
    }

  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    //...
  }
});
