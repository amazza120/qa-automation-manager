parasails.registerPage('manage-app-scripts', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    //...
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function (){
    _.extend(this, window.SAILS_LOCALS);

    // on load hide success message and app scripts drop down list
    $(document).ready(function() {
      $("#success").hide();
      $("#ddlAppAreasContainer").hide();
      $("#ddlAppScriptsContainer").hide();
      $("#btnCUDAppScript").prop( "disabled", true );
    });
  },
  mounted: async function() {

    // global page variables
    let appIdGlb = '';
    let appAreaIdGlb = '';

    // Create tab click
    $("#appScriptCreate").click(function() {
      $("#ddlAppAreasContainer").fadeOut();
      $("#ddlAppScriptsContainer").fadeOut();

      elementReset();

      $("#btnCUDAppScript").prop( "disabled", true );
      $("#btnCUDAppScript").removeClass('btn-danger');
      $("#btnCUDAppScript").addClass('btn-primary');
      $("#btnCUDAppScript").text('Create');
    });

    // Update tab click
    $("#appScriptUpdate").click(function() {
      $("#ddlAppAreasContainer").fadeOut();
      $("#ddlAppScriptsContainer").fadeOut();

      elementReset();

      $("#btnCUDAppScript").prop( "disabled", true );
      $("#btnCUDAppScript").removeClass('btn-danger');
      $("#btnCUDAppScript").addClass('btn-primary');
      $("#btnCUDAppScript").text('Update');
    });

    // Delete tab click
    $("#appScriptDelete").click(function() {
      $("#ddlAppAreasContainer").fadeOut();
      $("#ddlAppScriptsContainer").fadeOut();

      elementReset()

      $("#btnCUDAppScript").prop( "disabled", true );
      $("#btnCUDAppScript").removeClass('btn-primary');
      $("#btnCUDAppScript").addClass('btn-danger');
      $("#btnCUDAppScript").text('Delete');
    });

    $("#ddlApps a").click(function() {
      $('#ddlAppAreas').empty(); // empty element
      appIdGlb = $(this).attr("value");
      $.get('/get-app-area', {"app_identifier": appIdGlb}).then(resp => {
        $("#appIdentifier").val(appIdGlb);
        $("#ddlAppAreasContainer").fadeIn();
        resp.forEach(area => {
          $('#ddlAppAreas').append(`<a class="dropdown-item" value="${area._source.identifier}">${area._source.name}</a>`)
        });
      });
    });

    // Applications ddl select
    // User jquery 'on' format for dynamically inserted dom elements
    $("#ddlAppAreas").on('click', 'a', function() {
      $('#ddlAppScripts').empty(); // empty element
      appAreaIdGlb = $(this).attr("value");
      $.get('/get-app-script', {"app_identifier": appIdGlb, "app_area_identifier": appAreaIdGlb}).then(resp => {
        // Reason for switch: there is a possability of adding more options
        switch($("#btnCUDAppScript").text()) {
          case "Create":
            $("#appScriptName").prop( "disabled", false );
            $("#appScriptDescription").prop( "disabled", false );
            $("#appScriptFileName").prop( "disabled", false );
            $("#appIdentifier").val(appIdGlb);
            $("#appAreaIdentifier").val(appAreaIdGlb);
            $("#appScriptActive").prop("disabled", false );
            break;
          case "Update":
            $("#ddlAppScriptsContainer").fadeIn();
            break;
          case "Delete":
            $("#ddlAppScriptsContainer").fadeIn();
            break;
        }
        resp.forEach(script => {
          $('#ddlAppScripts').append(`<a class="dropdown-item" value="${script._source.file_name}">${script._source.name}</a>`)
        });
      });
    });

    // Applications ddl select
    // User jquery 'on' format for dynamically inserted dom elements
    $("#ddlAppScripts").on('click', 'a', function() {
      let selAppScript = $(this).attr("value");
      $.get('/get-app-script', {"file_name": selAppScript, "app_identifier": appIdGlb, "app_area_identifier": appAreaIdGlb}).then(resp => {
        // Reason for switch: there is a possability of adding more options
        switch($("#btnCUDAppScript").text()) {
          case "Update":
            $("#appScriptName").prop( "disabled", false );
            $("#appScriptDescription").prop( "disabled", false );
            $("#appScriptActive").prop("disabled", false );
            break;
          case "Delete":
            $("#btnCUDAppScript").prop( "disabled", false );
            break;
        }
        $("#appScriptName").val(resp.name);
        $("#appScriptDescription").val(resp.description);
        $("#appScriptFileName").val(resp.file_name);
        $("#appIdentifier").val(appIdGlb);
        $("#appAreaIdentifier").val(appAreaIdGlb);
        $("#appScriptActive").prop( "checked", resp.active );
      });
    });

    // Create / Update / Delete button click
    $("#btnCUDAppScript").click(function() {
      let nameVal = $("#appScriptName").val();
      let descVal = $("#appScriptDescription").val();
      let fileNameVal = $("#appScriptFileName").val();
      let appIdentifierVal = $("#appIdentifier").val();
      let appAreaIdentifierVal = $("#appAreaIdentifier").val();
      let activeVal = $("#appScriptActive").prop("checked");

      switch($("#btnCUDAppScript").text()) {
        case "Create":
          $.post('/create-app-script', {
            "name": nameVal,
            "description": descVal,
            "file_name": fileNameVal,
            "app_identifier": appIdentifierVal,
            "app_area_identifier": appAreaIdentifierVal,
            "active": activeVal
          }).then(resp => {
            console.log(resp);
            $("#success").text(`Document id: ${resp._id} Created!`);
            $("#success").fadeIn(function() {
              $("#success").fadeOut(2500, function() {
                // force reload to query the new app scripts
                location.reload();
              });
            });
          });
          break;
        case "Update":
          $.post('/update-app-script', {
            "name": nameVal,
            "description": descVal,
            "file_name": fileNameVal,
            "app_identifier": appIdentifierVal,
            "app_area_identifier": appAreaIdentifierVal,
            "active": activeVal
          }).then(resp => {
            console.log(resp);
            $("#success").text(`Document id: ${resp._id} Updated!`);
            $("#success").fadeIn(function() {
              $("#success").fadeOut(2500, function() {
                // force reload to query the new app scripts
                location.reload();
              });
            });
          });
          break;
        case "Delete":
          $.post('/delete-app-script', {"file_name": fileNameVal, "app_identifier": appIdentifierVal, "app_area_identifier": appAreaIdentifierVal}).then(resp => {
            console.log(resp);
            $("#success").text(`Document id: ${resp._id} Deleted!`);
            $("#success").fadeIn(function() {
              $("#success").fadeOut(2500, function() {
                // force reload to query the new app scripts
                location.reload();
              });
            });
          });
          break;
      }
    });

    // active validation
    $("#appScriptName,#appScriptFileName,#appScriptDescription,#appIdentifier,#appAreaIdentifier").on("input propertychange", function() {
      let fieldsFilled = $("#appScriptName").val().length > 0 
        && $("#appScriptFileName").val().length > 0
        && $("#appScriptDescription").val().length > 0
        && $("#appIdentifier").val().length > 0
        && $("#appAreaIdentifier").val().length > 0;
        
      if (fieldsFilled) {
        $("#btnCUDAppScript").prop( "disabled", false );
      }
      else {
        $("#btnCUDAppScript").prop( "disabled", true );
      }
    });

    function elementReset() {
      $("#appScriptName").prop( "disabled", true );
      $("#appScriptDescription").prop( "disabled", true );
      $("#appScriptFileName").prop( "disabled", true );
      $("#appScriptActive").prop("disabled", true );

      $("#appScriptName").val('');
      $("#appScriptDescription").val('');
      $("#appScriptFileName").val('');
      $("#appIdentifier").val('');
      $("#appAreaIdentifier").val('');
      $("#appScriptActive").prop("checked", false);
    }

  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    //...
  }
});
