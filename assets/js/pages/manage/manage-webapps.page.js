parasails.registerPage('manage-webapps', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    //...
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function (){
    _.extend(this, window.SAILS_LOCALS);

    // on load hide success message and applications drop down list
    $(document).ready(function() {
      $("#success").hide();
      $("#ddlAppsContainer").hide();
      $("#btnCUDWebApp").prop( "disabled", true );
    });
  },
  mounted: async function() {

    // Create tab click
    $("#webAppCreate").click(function() {
      $("#ddlAppsContainer").fadeOut();

      $("#webAppName").prop( "disabled", false );
      $("#webAppIdentifier").prop( "disabled", false );
      $("#webAppDefaultUrl").prop( "disabled", false );
      $("#webAppActive").prop( "disabled", false );
      $("#webAppProjectId").prop( "disabled", false );
      $("#webAppCypressKey").prop( "disabled", false );

      $("#webAppName").val('');
      $("#webAppIdentifier").val('');
      $("#webAppDefaultUrl").val('');
      $("#webAppProjectId").val('');
      $("#webAppCypressKey").val('');
      $("#webAppActive").prop( "checked", false );

      $("#btnCUDWebApp").prop( "disabled", true );
      $("#btnCUDWebApp").removeClass('btn-danger');
      $("#btnCUDWebApp").addClass('btn-primary');
      $("#btnCUDWebApp").text('Create');
    });

    // Update tab click
    $("#webAppUpdate").click(function() {
      $("#ddlAppsContainer").fadeIn();

      $("#webAppName").prop( "disabled", true );
      $("#webAppIdentifier").prop( "disabled", true );
      $("#webAppDefaultUrl").prop( "disabled", true );
      $("#webAppActive").prop( "disabled", true );
      $("#webAppProjectId").prop( "disabled", true );
      $("#webAppCypressKey").prop( "disabled", true );

      $("#btnCUDWebApp").prop( "disabled", true );
      $("#btnCUDWebApp").removeClass('btn-danger');
      $("#btnCUDWebApp").addClass('btn-primary');
      $("#btnCUDWebApp").text('Update');
    });

    // Delete tab click
    $("#webAppDelete").click(function() {
      $("#ddlAppsContainer").fadeIn();

      $("#webAppName").prop( "disabled", true );
      $("#webAppIdentifier").prop( "disabled", true );
      $("#webAppDefaultUrl").prop( "disabled", true );
      $("#webAppActive").prop( "disabled", true );
      $("#webAppProjectId").prop( "disabled", true );
      $("#webAppCypressKey").prop( "disabled", true );

      $("#btnCUDWebApp").prop( "disabled", true );
      $("#btnCUDWebApp").removeClass('btn-primary');
      $("#btnCUDWebApp").addClass('btn-danger');
      $("#btnCUDWebApp").text('Delete');
    });

    // Applications ddl select
    $("#ddlApps a").click(function() {
      let selApp = $(this).attr("value");
      $.get('/get-webapp', {"identifier": selApp}).then(resp => {
        // Reason for switch: there is a possability of adding more options
        switch($("#btnCUDWebApp").text()) {
          case "Update":
            $("#webAppName").prop( "disabled", false );
            $("#webAppDefaultUrl").prop( "disabled", false );
            $("#webAppActive").prop( "disabled", false );
            $("#webAppProjectId").prop( "disabled", false );
            $("#webAppCypressKey").prop( "disabled", false );
            $("#btnCUDWebApp").prop( "disabled", false );
            break;
          case "Delete":
            $("#btnCUDWebApp").prop( "disabled", false );
            break;
        }
        $("#webAppName").val(resp.name);
        $("#webAppIdentifier").val(resp.identifier);
        $("#webAppDefaultUrl").val(resp.default_url);
        $("#webAppProjectId").val(resp.project_id);
        $("#webAppCypressKey").val(resp.cypress_key);
        $("#webAppActive").prop( "checked", resp.active );
      });
    });

    // Create / Update / Delete button click
    $("#btnCUDWebApp").click(function() {
      let nameVal = $("#webAppName").val();
      let identifierVal = $("#webAppIdentifier").val();
      let defaultUrlVal = $("#webAppDefaultUrl").val();
      let projectIdVal = $("#webAppProjectId").val();
      let cypressKeyVal = $("#webAppCypressKey").val();
      let activeVal = $("#webAppActive").prop("checked");

      switch($("#btnCUDWebApp").text()) {
        case "Create":
          $.post('/create-webapp', {
            "name": nameVal, 
            "identifier": identifierVal, 
            "defaultUrl": defaultUrlVal,
            "projectId": projectIdVal,
            "cypressKey": cypressKeyVal,
            "active": activeVal
          }).then(resp => {
            console.log(resp);
            $("#success").text(`Document id: ${resp._id} Created!`);
            $("#success").fadeIn(function() {
              $("#success").fadeOut(5000, function() {
                // force reload to query the new applications
                location.reload();
              });
            });
          });
          break;
        case "Update":
          $.post('/update-webapp', {
            "name": nameVal, 
            "identifier": identifierVal, 
            "defaultUrl": defaultUrlVal,
            "projectId": projectIdVal,
            "cypressKey": cypressKeyVal,
            "active": activeVal
          }).then(resp => {
            console.log(resp);
            $("#success").text(`Document id: ${resp._id} Updated!`);
            $("#success").fadeIn(function() {
              $("#success").fadeOut(5000, function() {
                // force reload to query the new applications
                location.reload();
              });
            });
          });
          break;
        case "Delete":
          $.post('/delete-webapp', {"identifier": identifierVal}).then(resp => {
            console.log(resp);
            $("#success").text(`Document id: ${resp._id} Deleted!`);
            $("#success").fadeIn(function() {
              $("#success").fadeOut(5000, function() {
                // force reload to query the new applications
                location.reload();
              });
            });
          });
          break;
      }
    });

    // active validation
    $("#webAppName,#webAppIdentifier,#webAppDefaultUrl,#webAppProjectId,#webAppCypressKey").on("input propertychange", function() {
      let fieldsFilled = $("#webAppName").val().length > 0 
        && $("#webAppIdentifier").val().length > 0 
        && $("#webAppDefaultUrl").val().length > 0 
        && $("#webAppProjectId").val().length > 0 
        && $("#webAppCypressKey").val().length > 0;
        
      if (fieldsFilled) {
        $("#btnCUDWebApp").prop( "disabled", false );
      }
      else {
        $("#btnCUDWebApp").prop( "disabled", true );
      }
    });
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    //...
  }
});
