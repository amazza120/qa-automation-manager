parasails.registerPage('manage-browsers', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    //...
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function (){
    _.extend(this, window.SAILS_LOCALS);

    // on load hide success message and browsers drop down list
    $(document).ready(function() {
      $("#success").hide();
      $("#ddlBrowsersContainer").hide();
      $("#btnCUDBrowser").prop( "disabled", true );
    });
  },
  mounted: async function() {

    // Create tab click
    $("#browserCreate").click(function() {
      $("#ddlBrowsersContainer").fadeOut();

      $("#browserName").prop( "disabled", false );
      $("#browserIdentifier").prop( "disabled", false );
      $("#browserActive").prop("disabled", false );

      $("#browserName").val('');
      $("#browserIdentifier").val('');
      $("#browserActive").prop("checked", false);

      $("#btnCUDBrowser").prop( "disabled", true );
      $("#btnCUDBrowser").removeClass('btn-danger');
      $("#btnCUDBrowser").addClass('btn-primary');
      $("#btnCUDBrowser").text('Create');
    });

    // Update tab click
    $("#browserUpdate").click(function() {
      $("#ddlBrowsersContainer").fadeIn();

      $("#browserName").prop( "disabled", true );
      $("#browserIdentifier").prop( "disabled", true );
      $("#browserActive").prop("disabled", true );

      $("#btnCUDBrowser").prop( "disabled", true );
      $("#btnCUDBrowser").removeClass('btn-danger');
      $("#btnCUDBrowser").addClass('btn-primary');
      $("#btnCUDBrowser").text('Update');
    });

    // Delete tab click
    $("#browserDelete").click(function() {
      $("#ddlBrowsersContainer").fadeIn();

      $("#browserName").prop( "disabled", true );
      $("#browserIdentifier").prop( "disabled", true );
      $("#browserActive").prop("disabled", true );

      $("#btnCUDBrowser").prop( "disabled", true );
      $("#btnCUDBrowser").removeClass('btn-primary');
      $("#btnCUDBrowser").addClass('btn-danger');
      $("#btnCUDBrowser").text('Delete');
    });

    // Applications ddl select
    $("#ddlBrowsers a").click(function() {
      let selApp = $(this).attr("value");
      $.get('/get-browser', {"identifier": selApp}).then(resp => {
        // Reason for switch: there is a possability of adding more options
        switch($("#btnCUDBrowser").text()) {
          case "Update":
            $("#browserName").prop( "disabled", false );
            $("#browserActive").prop("disabled", false );
            $("#btnCUDBrowser").prop( "disabled", false );
            break;
          case "Delete":
            $("#btnCUDBrowser").prop( "disabled", false );
            break;
        }
        $("#browserName").val(resp.name);
        $("#browserIdentifier").val(resp.identifier);
        $("#browserActive").prop( "checked", resp.active );
      });
    });

    // Create / Update / Delete button click
    $("#btnCUDBrowser").click(function() {
      let nameVal = $("#browserName").val();
      let identifierVal = $("#browserIdentifier").val();
      let activeVal = $("#browserActive").prop("checked");

      switch($("#btnCUDBrowser").text()) {
        case "Create":
          $.post('/create-browser', {
            "name": nameVal, 
            "identifier": identifierVal, 
            "active": activeVal
          }).then(resp => {
            console.log(resp);
            $("#success").text(`Document id: ${resp._id} Created!`);
            $("#success").fadeIn(function() {
              $("#success").fadeOut(2500, function() {
                // force reload to query the new browsers
                location.reload();
              });
            });
          });
          break;
        case "Update":
          $.post('/update-browser', {
            "name": nameVal, 
            "identifier": identifierVal, 
            "active": activeVal
          }).then(resp => {
            console.log(resp);
            $("#success").text(`Document id: ${resp._id} Updated!`);
            $("#success").fadeIn(function() {
              $("#success").fadeOut(2500, function() {
                // force reload to query the new browsers
                location.reload();
              });
            });
          });
          break;
        case "Delete":
          $.post('/delete-browser', {"identifier": identifierVal}).then(resp => {
            console.log(resp);
            $("#success").text(`Document id: ${resp._id} Deleted!`);
            $("#success").fadeIn(function() {
              $("#success").fadeOut(2500, function() {
                // force reload to query the new browsers
                location.reload();
              });
            });
          });
          break;
      }
    });

    // active validation
    $("#browserName,#browserIdentifier").on("input propertychange", function() {
      let fieldsFilled = $("#browserName").val().length > 0 
        && $("#browserIdentifier").val().length > 0;
        
      if (fieldsFilled) {
        $("#btnCUDBrowser").prop( "disabled", false );
      }
      else {
        $("#btnCUDBrowser").prop( "disabled", true );
      }
    });
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    //...
  }
});
