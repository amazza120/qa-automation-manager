parasails.registerPage('homepage', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    heroHeightSet: false,
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
  },
  mounted: async function(){
    this._setHeroHeight();

    // render google login button
    setTimeout(function() { 
      gapi.signin2.render('gSignInHome', {
        'longtitle': true,
        'theme': 'dark'
      });
    }, 1500);
    
    $('#gSignInHome').click(function() {
      gapi.load('auth2', function() {
        gapi.auth2.init({
          client_id: '396819228577-1l0ai19j3h8ontok6ki7boh190eejhf8.apps.googleusercontent.com',
        }).then(auth2 => {
          auth2.grantOfflineAccess({
            scope: 'https://www.googleapis.com/auth/gmail.readonly'
          }).then(appAuth); 
        });
      });  
    });

    async function appAuth(authResult) {
      if (authResult['code']) {
        setTimeout(async function() { 
          let googleUser = gapi.auth2.getAuthInstance().currentUser.get();
          let profile = googleUser.getBasicProfile();
  
          const email = profile.getEmail();
          const name = profile.getName();
  
          const sailsAuthResp = await $.get('/sails-auth', { 
            "email": email, 
            "name": name 
          });
  
          if (sailsAuthResp === 'OK') {
            localStorage.setItem("isSignedIn", "true");
            if (email === 'amazza@64labs.com') {
              const cronResp = await $.get('/start-cron-job', { "gAuthCode": authResult['code'] });
              console.log(cronResp);
            }
            window.location = '/welcome';
          }
        }, 3000);
      }
    }
    
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    clickHeroButton: async function() {
      // Scroll to the 'get started' section:
      $('html, body').animate({
        scrollTop: this.$find('[role="scroll-destination"]').offset().top
      }, 500);
    },

    // Private methods not tied to a particular DOM event are prefixed with _
    _setHeroHeight: function() {
      var $hero = this.$find('[full-page-hero]');
      var headerHeight = $('#page-header').outerHeight();
      var heightToSet = $(window).height();
      heightToSet = Math.max(heightToSet, 500);//« ensure min height of 500px - header height
      heightToSet = Math.min(heightToSet, 1000);//« ensure max height of 1000px - header height
      $hero.css('min-height', heightToSet - headerHeight+'px');
      this.heroHeightSet = true;
    },

  }
});
