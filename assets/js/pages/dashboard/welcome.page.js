parasails.registerPage('welcome', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    dashboardModalNoticeVisible: false,
    dashboardModalReferenceVisible: false
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);

    $("#spinner").hide();
    $("#testStatus").hide();
    $("#testResultUrl").hide();
    $("#deviceContainer").hide();
    $("#urlContainer").hide();
    $("#appContainer").hide();
    $("#appAreaContainer").hide();
    $("#scriptContainer").hide();
    $("#browserContainer").hide();
    $("#testAppContainer").hide();
    $("#jiraIssueContainer").hide();
    $("#errorMsg").hide();
    $("#btnGrpDevices").hide();
    $("#btnGrpBrowsers").hide();
    $("#btnGrpAppAreas").hide();
    $("#btnGrpScripts").hide();
    $("#resultAccordion").hide();
  },
  mounted: async function() {

    let appIdGlb = ''; // global app id
    let appAreaIdGlb = ''; // global app area id
    let deviceIdGlb = ''; // global device id
    let browserIdGlb = ''; // global browser id
    let scriptIdGlb = ''; // global script id
    let appProjectId = ''; // application project Id

    // step 1, choose an application
    // applications dropdown list actions
    $("#ddlApps a").click(function() {

      resetPage();

      let selApp = $(this).text();
      let selUrl = $(this).attr("url");

      appProjectId = $(this).attr("projectId"); // save to global variable for later use
      appIdGlb = $(this).attr("value"); // save to global variable for later use

      $("#txtApp").val(selApp);
      $("#txtUrl").val(selUrl);
    
      $("#appContainer").show("fast");
      $("#urlContainer").show("fast");

      $("#btnGrpDevices").show("fast");
      
      if (selApp === 'Testing') {
        $("#testAppContainer").show("fast");
      }
      else {
        $("#testAppContainer").hide();
      }

      if (selApp === 'Pandora') {
        $('#urlPrefix').text('http://us.');
      }
      else {
        $('#urlPrefix').text('http://www.');
      }

      // Fill in app area drop down list based on selection
      $('#ddlAppAreas').empty(); // empty element
      $.get('/get-app-area', {"app_identifier": appIdGlb, "active":true}).then(resp => {
        resp.forEach(area => {
          $('#ddlAppAreas').append(`<a class="dropdown-item" value="${area._source.identifier}">${area._source.name}</a>`)
        });
      });

      // continue to step 2 - pick a device
      $("#pickParamsDesc").html("Pick a device:");
    });

    // step 2, choose a device
    // device dropdown list actions
    $("#ddlDevices a").click(async function() {
      scrollToBottom();
      let selDevice = $(this).text();
      $("#txtDevice").val(selDevice);

      deviceIdGlb = $(this).attr("value");

      $("#btnGrpBrowsers").show("fast");
      $("#btnGrpAppAreas").show("fast");
      $("#deviceContainer").show("fast");
      $("#jiraIssueContainer").show("fast");

      if (await validateForm()) {
        $("#btnRun").prop('disabled', false);
        $("#btnGenScript").prop('disabled', false);
      }

      // generate a quick script and/or run the test
      $("#pickParamsDesc").html(`Test run is ready! 
        <br><br> The following steps are optional:
        <br> - Pick a specific area to test or script to execute. By default, all scripts will be executed for the chosen application. 
        <br> - Add a Jira ticket. The results will be posted as a comment when the test run is complete.
        <br> - Pick a browser. Default is Electron. Chrome does not support video recording. 
        <br> - You can now generate a quick script of this test run for future use.
      `);
          
    });

    // optional step, by default electon is chosen. If the user wishes, they can change it to chrome
    // browser dropdown list actions
    $("#ddlBrowsers a").click(function() {
      let selBrowser = $(this).text();
      $("#txtBrowser").val(selBrowser);
      browserIdGlb = $(this).attr("value");
      $("#browserContainer").show("fast");
    });

    // optional step, by default there will be application area chosen and all scripts will be excuted for that application
    // User jquery 'on' format for dynamically inserted dom elements
    $("#ddlAppAreas").on('click', 'a', function() {
      scriptIdGlb = '';
      $("#scriptContainer").fadeOut();
      $('#ddlAppScripts').empty(); // empty element
      let appAreaName = $(this).text();
      appAreaIdGlb = $(this).attr("value");
      $.get('/get-app-script', {"app_identifier": appIdGlb, "app_area_identifier": appAreaIdGlb, "active": true}).then(resp => {
        $("#btnGrpScripts").show("fast");
        $("#appAreaContainer").show("fast");
        $("#txtAppArea").val(appAreaName);
        resp.forEach(script => {
          $('#ddlAppScripts').append(`
            <a class="dropdown-item" title="${script._source.description}" value="${script._source.file_name}">${script._source.name}</a>
          `);
        });
      });
    });

    // optional step, by default there will be no script chosen and all scripts under the application area will be executed
    // User jquery 'on' format for dynamically inserted dom elements
    $("#ddlAppScripts").on('click', 'a', function() {
      let selScript = $(this).text();
      $("#txtScript").val(selScript);
      scriptIdGlb = $(this).attr("value");
      $("#scriptContainer").show("fast");
    });

    // optional step, by default the Jira issue is blank. If the user wishes, they add a ticket and we will verify if it exists
    // Jira issue validation
    $("#txtJiraIssue").on('blur', function() {
      jiraValidation();
    });

    // generate quick run script button action
    $("#btnGenScript").click(async function() {
      let script = '';
      let appArea = '';
      let file = '';
      let state = await pageState();

      // optional - app area
      if ($('#txtAppArea').val().length > 0) {
        appArea = `, "appArea":"${appAreaIdGlb}"`;
      }

      // optional - file
      if ($('#txtScript').val().length > 0) {
        file = `, "script":"${scriptIdGlb}"`;
      }

      switch (state) {
        case 'testing':
          script = `{"application":"${appIdGlb}", "testApp":"${$("#txtTestApp").val()}", "browser":"${browserIdGlb}", "url":"${$("#txtUrl").val()}", "device":"${deviceIdGlb}"${appArea}${file}}`;
          break;
        case 'jira':
          script = `{"application":"${appIdGlb}", "browser":"${browserIdGlb}", "url":"${$("#txtUrl").val()}", "device":"${deviceIdGlb}", "jiraIssueKey":"${$("#txtJiraIssue").val()}"${appArea}${file}}`;
          break;
        case 'both':
          script = `{"application":"${appIdGlb}", "testApp":"${$("#txtTestApp").val()}", "browser":"${browserIdGlb}", "url":"${$("#txtUrl").val()}", "device":"${deviceIdGlb}", "jiraIssueKey":"${$("#txtJiraIssue").val()}"${appArea}${file}}`;
          break;
        case 'none':
          script = `{"application":"${appIdGlb}", "browser":"${browserIdGlb}", "url":"${$("#txtUrl").val()}", "device":"${deviceIdGlb}"${appArea}${file}}`;
          break;
      }

      $("#txtRunParams").val(script);
      $("#btnRun").prop('disabled', false);
    });

    // alter run button enabled property depending on if "quick run" parameters exist
    $("#txtRunParams").on("input propertychange", async function() {
      let validParams = await validateForm();
      if ($(this).val().length == 0 && validParams ) {
        // special case when the quick script input is used but emptied to run the the chosen parameters
        $("#errorMsg").fadeOut("fast");
        $("#btnRun").prop('disabled', false);
      }
      else {
        let params = validateScript($(this).val());

        if (params === "error") {
          $("#errorMsg").text("Invalid Quick Run Script!");
          $("#errorMsg").fadeIn("fast");
  
          $("#btnRun").prop('disabled', true);
        }
        else {
          $("#errorMsg").fadeOut("fast");
          $("#btnRun").prop('disabled', false);
        }
      }
    });

    $("#txtUrl,#txtTestApp").on("input propertychange", async function() {
      if (await validateForm()) {
        $("#btnRun").prop('disabled', false);
        $("#btnGenScript").prop('disabled', false);
      }
      else {
        $("#btnRun").prop('disabled', true);
        $("#btnGenScript").prop('disabled', true);
      }
    });
    
    // run button actions
    $("#btnRun").click(function() {
      let url = '';
      let testAppParam = '';
      let jiraIssueKey = '';

      updateResultElements("running");

      if ($("#txtRunParams").val().length > 0) {
        let jsonData = validateScript($("#txtRunParams").val());
        $.ajax({
          url: '/run-test',
          type: 'post',
          data: {
            user: 'QA Automation Manager', 
            device: jsonData.device, 
            application: jsonData.application, 
            testApp: jsonData.testApp ? jsonData.testApp : '',
            url: jsonData.url,
            browser: jsonData.browser,
            appArea: jsonData.appArea ? jsonData.appArea : '',
            script: jsonData.script ? jsonData.script : '',
            jiraIssueKey: jsonData.jiraIssueKey,
            timeout: 0
          },
          headers: {
            "64-api-key": 'lFe4FjDjLOrjTnw9hhwe5jathKvCvYu'
          },
          dataType: 'json'
        })
        .then(resp => {
          console.log(resp);
          generatingReports(resp.url.url);
          setTimeout(function(){
            if (resp.url.passed) {
              updateResultElements("complete", resp);
            }
            else {
              updateResultElements("error", resp);
            }
          }, 5000);
        });
        /*
        $.post('/run-test', {
          "user": 'QA Automation Manager', 
          "device": jsonData.device, 
          "application": jsonData.application, 
          "testApp": jsonData.testApp ? jsonData.testApp : '',
          "url": jsonData.url,
          "browser": jsonData.browser,
          "appArea": jsonData.appArea ? jsonData.appArea : '',
          "script": jsonData.script ? jsonData.script : '',
          "jiraIssueKey": jsonData.jiraIssueKey,
          "timeout": 0
        }).then(resp => {
          console.log(resp);
          generatingReports(resp.url.url);
          setTimeout(function(){
            if (resp.url.passed) {
              updateResultElements("complete", resp);
            }
            else {
              updateResultElements("error", resp);
            }
          }, 5000);
        });
        */
      }
      else {
        url = $("#txtUrl").val();
        testAppParam = $("#txtTestApp").val();
        jiraIssueKey = $("#txtJiraIssue").val();
        $.ajax({
          url: '/run-test',
          type: 'post',
          data: {
            user: 'QA Automation Manager', 
            device: deviceIdGlb, 
            application: appIdGlb, 
            testApp: testAppParam ? testAppParam : '',
            url: url,
            browser: browserIdGlb,
            appArea: appAreaIdGlb ? appAreaIdGlb : '',
            script: scriptIdGlb ? scriptIdGlb : '',
            jiraIssueKey: jiraIssueKey ? jiraIssueKey : '',
            timeout: 0
          },
          headers: {
            "64-api-key": 'lFe4FjDjLOrjTnw9hhwe5jathKvCvYu'
          },
          dataType: 'json'
        })
        .then(resp => {
          console.log(resp);
          generatingReports(resp.url.url);
          setTimeout(function(){
            if (resp.url.passed) {
              updateResultElements("complete", resp);
            }
            else {
              updateResultElements("error", resp);
            }
          }, 5000);
        });
        /*
        $.post('/run-test', {
          "user": 'QA Automation Manager', 
          "device": deviceIdGlb, 
          "application": appIdGlb, 
          "testApp": testAppParam ? testAppParam : '',
          "url": url,
          "browser": browserIdGlb,
          "appArea": appAreaIdGlb ? appAreaIdGlb : '',
          "script": scriptIdGlb ? scriptIdGlb : '',
          "jiraIssueKey": jiraIssueKey ? jiraIssueKey : '',
          "timeout": 0
        }).then(resp => {
          console.log(resp);
          generatingReports(resp.url.url);
          setTimeout(function(){
            if (resp.url.passed) {
              updateResultElements("complete", resp);
            }
            else {
              updateResultElements("error", resp);
            }
          }, 5000);
        });
        */
      }

    });

    // page state
    async function pageState() {
      $("#spinner").fadeIn();
      if ($("#txtApp").val() === 'Testing' && await jiraValidation()) {
        $("#spinner").fadeOut();
        return 'both';
      }
      else if (await jiraValidation()) {
        $("#spinner").fadeOut();
        return 'jira';
      }
      else if ($("#txtApp").val() === 'Testing') {
        $("#spinner").fadeOut();
        return 'testing';
      }
      else {
        $("#spinner").fadeOut();
        return 'none';
      }
    }

    // validate the form
    async function validateForm() {
      let state = await pageState();
      switch (state) {
        case 'testing':
          return $("#txtApp").val().length > 0 && $("#txtTestApp").val().length > 0 && $("#txtUrl").val().length > 0 && $("#txtBrowser").val().length > 0 && $("#txtDevice").val().length > 0;
        case 'jira':
          return $("#txtApp").val().length > 0 && $("#txtUrl").val().length > 0 && $("#txtBrowser").val().length > 0 && $("#txtDevice").val().length > 0 && $("#txtJiraIssue").val().length > 0;
        case 'both':
          return $("#txtApp").val().length > 0 && $("#txtTestApp").val().length > 0 && $("#txtUrl").val().length > 0 && $("#txtBrowser").val().length > 0 && $("#txtDevice").val().length > 0 && $("#txtJiraIssue").val().length > 0;
        case 'none':
          return $("#txtApp").val().length > 0 && $("#txtUrl").val().length > 0 && $("#txtBrowser").val().length > 0 && $("#txtDevice").val().length > 0;
      }
    }

    // format the quick script
    function validateScript(script) {
      try {
        const regex = /{(.*)}/gm
        const quickScript = JSON.parse(regex.exec(script)[0]);

        // first level is check if it is a test script or not
        if ($("#txtApp").val() === 'Testing') {
          if (quickScript.application && quickScript.testApp && quickScript.url && quickScript.browser && quickScript.device) {
            return validateOptionalSettings(quickScript);
          }
          else {
            // catch errors if the necessary fields are not present
            return "error";
          }
        }
        else {
          // if all of these keys exist in the object continue
          if (quickScript.application && quickScript.url && quickScript.browser && quickScript.device) {
            return validateOptionalSettings(quickScript);
          }
          else {
            // catch errors if the necessary fields are not present
            return "error";
          }
        }
      }
      catch (err) {
        // catch any json related format issues
        return "error";
      }
    }

    function validateOptionalSettings(quickScript) {
      if ($("#txtAppArea").val().length > 0) {
        if (!quickScript.appArea) {
          return "error";
        }
      }
      if ($("#txtScript").val().length > 0) {
        if (!quickScript.script) {
          return "error";
        }
      }
      return quickScript;
    }

    async function jiraValidation() {
      let jiraIssue = $('#txtJiraIssue').val();
      return await $.get('/get-jira-issue', {"jiraIssueKey": jiraIssue}).then(resp => {
        if (resp) {
          $('#txtJiraIssue').removeClass('error');
          $('#txtJiraIssue').addClass('pass');
          return true;
        } 
        else {
          $('#txtJiraIssue').removeClass('pass');
          $('#txtJiraIssue').addClass('error');
          return false;
        }
      });
    }

    // update UI elements based on parameters
    function updateResultElements(state, data) {
      switch(state) {
        case "running":
          scrollToBottom();
          $("#spinner").fadeOut();
          $('#accResultsBody').empty();
          $("#resultAccordion").fadeOut();
          $('#collapseResults').collapse('hide');
          $("#testStatus").fadeOut(1500, function() {
            $("#testStatus").removeClass("error");
            $("#testStatus").text("Your tests are running! View the status of your test run here: ");
          });
          $("#testResultUrl").fadeOut(2000, function() {
            $("#testResultUrl").attr("href", `https://dashboard.cypress.io/#/projects/${appProjectId}/runs?page=1&status=running`);
            $("#testResultUrl").text(`https://dashboard.cypress.io/#/projects/${appProjectId}/runs?page=1&status=running`);
          });
          $("#spinner").fadeIn();
          $("#testStatus").fadeIn(1500);
          $("#testResultUrl").fadeIn(2000);
          $("#btnRun").prop('disabled', true);
          break;
        case "complete":
          // there could be multiple reports, go through and add them
          data.report.forEach(report => {
            $("#accResultsBody").append(`
              <div class="resp-container">
                <iframe class="resp-iframe" src="${report}"></iframe>
              </div>
            `);
          });
          // set result detail body and show
          $("#accResultsBody").append(`
            <br>
            <p class='pass'>All tests passed!</p>
            <p>For more details on the test run visit <a href="${data.url.url}" target="_blank">64labs Cypress Dashboard</a> and <a href="https://5e38df3ed59442238fadc9620181656b.us-east-1.aws.found.io:9243/app/kibana#/discover?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-5y,mode:quick,to:now))&_a=(columns:!(_source),index:'0b818e10-d614-11e8-a0cb-d542d74c311d',interval:auto,query:(language:lucene,query:'run_id:${data.uuid}'),sort:!(date_recorded,desc))" target="_blank">Kibana Test Result Database</a></p>
          `);
          $('#collapseResults').collapse('show');

          // change UI for user
          scrollToBottom();
          $("#spinner").hide();
          $("#testStatus").fadeOut(1500, function() {
            $("#testStatus").text("Your test run completed with no errors! View the results here: ");
          });
          $("#testStatus").fadeIn(1500);
          $("#resultAccordion").fadeIn(5000);
          $("#btnRun").prop('disabled', false);
          break;
        case "error":
          // there could be multiple reports, go through and add them
          data.report.forEach(report => {
            $("#accResultsBody").append(`
              <div class="resp-container">
                <iframe class="resp-iframe" src="${report}"></iframe>
              </div>
            `);
          });
          // set result detail body and show
          $("#accResultsBody").append(`
            <br>
            <p class='error'>An error has occurred.</p>
            <p>For more details on the test run visit <a href="${data.url.url}" target="_blank">64labs Cypress Dashboard</a> and <a href="https://5e38df3ed59442238fadc9620181656b.us-east-1.aws.found.io:9243/app/kibana#/discover?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-5y,mode:quick,to:now))&_a=(columns:!(_source),index:'0b818e10-d614-11e8-a0cb-d542d74c311d',interval:auto,query:(language:lucene,query:'run_id:${data.uuid}%20AND%20result:failed'),sort:!(date_recorded,desc))" target="_blank">Kibana Test Result Database</a></p>
          `);
          $('#collapseResults').collapse('show');

          // change UI for user
          scrollToBottom();
          $("#spinner").hide();
          $("#testStatus").fadeOut(1500, function() {
            $("#testStatus").text("Your test run completed with errors. View the results here: ");
          });
          $("#testStatus").fadeIn(1500);
          $("#resultAccordion").fadeIn(5000);
          $("#btnRun").prop('disabled', false);
          break;
      }
    }

    function scrollToBottom() {
      $("html, body").animate({ scrollTop: $(document).height() }, 5000);
    }

    function generatingReports(url) {
      $("#testStatus").fadeOut(1500, function() {
        $("#testStatus").text("generating reports...");
      });
      $("#testStatus").fadeIn(1500);
      $('#testResultUrl').fadeOut(1500, function() {
        $("#testResultUrl").attr("href", url);
        $("#testResultUrl").text(url);
      });
      $('#testResultUrl').fadeIn(1500);
    }

    function resetPage() {
      appIdGlb = ''; // global app id
      appAreaIdGlb = ''; // global app area id
      deviceIdGlb = ''; // global device id
      scriptIdGlb = ''; // global script id
      appProjectId = ''; // application project Id
      browserIdGlb = 'electron'; // global browser id

      $("#spinner").hide();
      $("#deviceContainer").hide();
      $("#browserContainer").hide();
      $("#urlContainer").hide();
      $("#appContainer").hide();
      $("#appAreaContainer").hide();
      $("#scriptContainer").hide();
      $("#testAppContainer").hide();
      $("#jiraIssueContainer").hide();
      $("#errorMsg").hide();
      $("#btnGrpDevices").hide();
      $("#btnGrpBrowsers").hide();
      $("#btnGrpAppAreas").hide();
      $("#btnGrpScripts").hide();

      $("#txtDevice").val('');
      $("#txtTestApp").val('');
      $("#txtAppArea").val('');
      $("#txtScript").val('');
      $("#txtBrowser").val('Electron');
      $("#txtBrowser").attr('value', 'electron');

      $("#txtRunParams").val('');

      $("#btnRun").prop('disabled', true);
      $("#btnGenScript").prop('disabled', true);

      $("#pickParamsDesc").text("Pick an Application:");
    }

  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    clickOpenDashboardNoticeModalButton: async function() {
      this.dashboardModalNoticeVisible = true;
    },

    closeDashboardNoticeModal: async function() {
      this.dashboardModalNoticeVisible = false;
    },

    clickOpenDashboardReferenceModalButton: async function() {
      this.dashboardModalReferenceVisible = true;
    },

    closeDashboardReferenceModal: async function() {
      this.dashboardModalReferenceVisible = false;
    },

  }
});
