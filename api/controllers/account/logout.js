module.exports = {


  friendlyName: 'Logout',


  description: 'Logout',


  inputs: {
    
  },

  fn: async function (inputs, exits) {

    this.res.clearCookie('sails.sid', { path: '/' });

    return exits.success();

  }

};
