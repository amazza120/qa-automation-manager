module.exports = {


  friendlyName: 'View Kibana Projects',


  description: 'Display "Kibana Projects" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/kibana/menu/kibana-projects',
    }

  },


  fn: async function (inputs, exits) {

    return exits.success();

  }


};
