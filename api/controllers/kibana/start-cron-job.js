module.exports = {


  friendlyName: 'Start Cron Job',


  description: 'Start Cron Job',


  inputs: {
    gAuthCode: {
      description: 'Google Authorization Code',
      example: '4/yU4cQZTMnnMtetyFcIWNItG32eKxxxgXXX-Z4yyJJJo.4qHskT-UtugceFc0ZRONyF4z7U4UmAI'
    },
  },

  fn: async function (inputs, exits) {

    const resp = await sails.hooks.cron.startRequest(inputs.gAuthCode);
  
    return exits.success(resp);

  }

};
