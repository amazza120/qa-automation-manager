module.exports = {


  friendlyName: 'View Pandora Canvas',


  description: 'Display our Pandora kibana dashboard page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/kibana/canvas/pandora-canvas',
    }

  },


  fn: async function (inputs, exits) {

    return exits.success();

  }


};
