module.exports = {


  friendlyName: 'View Pandora Menu',


  description: 'Display "Pandora Menu" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/kibana/menu/pandora-menu',
    }

  },


  fn: async function (inputs, exits) {

    return exits.success();

  }


};
