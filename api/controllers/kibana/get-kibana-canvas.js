module.exports = {


  friendlyName: 'Get Kibana Canvas',


  description: 'Get Kibana Canvas',


  inputs: {
    
  },

  fn: async function (inputs, exits) {

    // get a list of canvas report messages within the past day and limit it to the latest result
    const latestMessage = await sails.hooks.gapi.gapiRequest('list', 'from:noreply@watcheralert.found.io subject:Test Canvas Report - Beta newer_than:1d', 'messages/id', {}, 1);

    // console.log(`get-kibana-canvas -> latestMessage: ${latestMessage.messages[0].id}`);

    // get attachment id on the latest canvas report message
    const messageDetails = await sails.hooks.gapi.gapiRequest('get', '', '', {id: latestMessage.messages[0].id}, '');

    // console.log(`get-kibana-canvas -> messageDetails: ${messageDetails.payload.parts[1].body.attachmentId}`);

    // get the message attachement
    const attachmentData = await sails.hooks.gapi.gapiRequest('attachments', '', '', {messageId: latestMessage.messages[0].id, attachmentId: messageDetails.payload.parts[1].body.attachmentId}, '');
  
    console.log(`get-kibana-canvas -> attachmentData: ${attachmentData}`);

    return exits.success();

  }

};
