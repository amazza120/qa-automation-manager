module.exports = {


  friendlyName: 'View mackage dashboard',


  description: 'Display our mackage kibana dashboard page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/kibana/dashboard/mackage-dashboard',
    }

  },


  fn: async function (inputs, exits) {

    return exits.success();

  }


};
