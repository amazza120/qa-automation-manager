module.exports = {


  friendlyName: 'View general dashboard',


  description: 'Display our general kibana dashboard page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/kibana/dashboard/general-dashboard',
    }

  },


  fn: async function (inputs, exits) {

    return exits.success();

  }


};
