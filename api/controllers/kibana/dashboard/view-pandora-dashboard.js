module.exports = {


  friendlyName: 'View Pandora Dashboard',


  description: 'Display our Pandora kibana dashboard page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/kibana/dashboard/pandora-dashboard',
    }

  },


  fn: async function (inputs, exits) {

    return exits.success();

  }


};
