module.exports = {


  friendlyName: 'View carparts dashboard',


  description: 'Display our Carparts kibana dashboard page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/kibana/dashboard/carparts-dashboard',
    }

  },


  fn: async function (inputs, exits) {

    return exits.success();

  }


};
