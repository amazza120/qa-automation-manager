module.exports = {


  friendlyName: 'View soia kyo dashboard',


  description: 'Display our Soia & Kyo kibana dashboard page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/kibana/dashboard/soia-kyo-dashboard',
    }

  },


  fn: async function (inputs, exits) {

    return exits.success();

  }


};
