module.exports = {


  friendlyName: 'View ann summers dashboard',


  description: 'Display our ann summers kibana dashboard page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/kibana/dashboard/ann-summers-dashboard',
    }

  },


  fn: async function (inputs, exits) {

    return exits.success();

  }


};
