module.exports = {


  friendlyName: 'View youfit dashboard',


  description: 'Display our youfit kibana dashboard page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/kibana/dashboard/youfit-dashboard',
    }

  },


  fn: async function (inputs, exits) {

    return exits.success();

  }


};
