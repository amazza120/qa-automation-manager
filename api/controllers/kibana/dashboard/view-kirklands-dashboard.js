module.exports = {


  friendlyName: 'View kirklands dashboard',


  description: 'Display our Kirlands kibana dashboard page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/kibana/dashboard/kirklands-dashboard',
    }

  },


  fn: async function (inputs, exits) {

    return exits.success();

  }


};
