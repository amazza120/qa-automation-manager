module.exports = {


  friendlyName: 'Sails Auth',


  description: 'Sails Auth',


  inputs: {

    email: {
      description: 'email address',
      example: 'amazza@64labs.com'
    },

    name: {
      description: 'full name',
      example: 'Andrew Mazza'
    },

  },

  fn: async function (inputs, exits) {

    console.log(inputs.email);
    console.log(inputs.name);
   
    // authorize
    this.req.session.userId = inputs.email;

    return exits.success();

  }

};
