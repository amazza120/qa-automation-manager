module.exports = {

  friendlyName: 'View automation dashboard page',

  description: 'Load all of the neccessary data for running automated tests, then display the page',

  exits: {

    success: {
      viewTemplatePath: 'pages/dashboard/welcome',
      description: 'Display the dashboard page for authenticated users.'
    },

  },

  fn: async function (inputs, exits) {

    // get device, app and browser lists from elasticsearch, if error is found, throw a server error
    let testDevices = await sails.hooks.escall.esRequest('search', 'devices', 'device', {q:'active:true'}, {s:'identifier:desc'});
    testDevices = testDevices.hits.hits;

    let testApps = await sails.hooks.escall.esRequest('search', 'applications', 'application', {q:'active:true'}, {s:'identifier:desc'});
    testApps = testApps.hits.hits;

    let browsers = await sails.hooks.escall.esRequest('search', 'browsers', 'browser', {q:'active:true'}, {s:'identifier:desc'});
    browsers = browsers.hits.hits;

    return exits.success({devices: testDevices, applications: testApps, browsers: browsers});

  }

};
