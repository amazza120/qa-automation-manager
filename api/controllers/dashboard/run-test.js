module.exports = {

    friendlyName: 'Run Test',
  
    description: 'Run Test',
  
    inputs: {
  
      device: {
        description: 'test device',
        example: 'iPhone X'
      },

      application: {
        description: 'application being tested',
        example: 'Youfit'
      },

      testApp: {
        description: 'optional field - use for the cypress testing project',
        example: 'youfit'
      },

      url: {
        description: 'url to be used in testing',
        example: 'https://youfit.com/'
      },

      user: {
        description: 'user running the test',
        example: 'Andrew Mazza'
      },

      browser: {
        description: 'cypress currently supports chrome and electron',
        example: 'electron'
      },

      appArea: {
        description: 'application area',
        example: 'cart'
      },

      script: {
        description: 'file name of the test script',
        example: 'test-the-cart.spec.js'
      },

      jiraIssueKey: {
        description: 'jira issue key',
        example: 'ASUM-378'
      }

    },
  
    fn: async function (inputs, exits) {

        // Using action2 syntax - https://sailsjs.com/documentation/concepts/actions-and-controllers
        // Note that machine-as-action provides actions with access to the request object as this.req
        // This configures this request to have no timeout
        this.req.setTimeout(0);

        let deviceDetails = await sails.hooks.escall.esRequest('search', 'devices', 'device', {q:`identifier:"${inputs.device}"`}, {s:'identifier:desc'});
        let appDetails = await sails.hooks.escall.esRequest('search', 'applications', 'application', {q:`identifier:"${inputs.application}"`}, {s:'identifier:desc'});

        deviceDetails = deviceDetails.hits.hits[0]._source;
        appDetails = appDetails.hits.hits[0]._source;

        let runTestOptions = {
          device: {
            name: deviceDetails.name,
            width: deviceDetails.width,
            height: deviceDetails.height
          },
          application: {
            name: appDetails.name,
            testApp: inputs.testApp,
            identifier: appDetails.identifier,
            cypressKey: appDetails.cypress_key
          },
          testUrl: inputs.url,
          user: inputs.user,
          browser: inputs.browser,
          appArea: inputs.appArea,
          spec: inputs.script,
          jiraIssueKey: inputs.jiraIssueKey
        }

        // execute user test run
        let results = await sails.hooks.runtest.runTest(runTestOptions);
    
        // parse out the cypress URL from the test results
        let testRunUrl = await sails.hooks.cypress.getCypressUrl(results.uuid, appDetails.project_id, results.stdout);

        // parse out the mochawesome report
        let reportPath = await sails.hooks.reporter.getReportPath(results.stdout);

        results = Object.assign(reportPath, results);
  
        // update the jira ticket only if a key was provided
        if (inputs.jiraIssueKey) {
          let pretext = testRunUrl.passed ? `${appDetails.name} CI Test Run Passed!` : `${appDetails.name} CI Test Run Failed!`;

          // uses https://jira.atlassian.com/secure/WikiRendererHelpAction.jspa?section=all&_ga=2.133537036.369080900.1537378570-558116761.1536937409
          let comment = `${pretext}
          
          View your test results on [Cypress|${testRunUrl.url}] and [Kibana|https://5e38df3ed59442238fadc9620181656b.us-east-1.aws.found.io:9243/app/kibana#/discover?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-5y,mode:quick,to:now))&_a=(columns:!(_source),index:'0b818e10-d614-11e8-a0cb-d542d74c311d',interval:auto,query:(language:lucene,query:'run_id:${results.uuid}'),sort:!(date_recorded,desc))]
          
          Have questions regarding the results? Reach out to the QA team via [Slack|https://64labs.slack.com/messages/CCAA1MEKW] or [mailto:amazza@64labs.com]`;

          let jiraCommentResp = await sails.hooks.jiracall.jiraRequest('post', `/rest/api/2/issue/${inputs.jiraIssueKey}/comment`, { data: { body: comment } });
          results = Object.assign({jiraResp: jiraCommentResp}, results);
        }

        return exits.success(Object.assign({url: testRunUrl}, results));
  
    }
  
  
  };
  