module.exports = {


    friendlyName: 'Create Browser',
  
  
    description: 'Create Browser',
  
  
    inputs: {
  
      name: {
        description: 'test browser',
        example: 'iPhone X'
      },

      identifier: {
        description: 'test browser identifier',
        example: 'iphonex'
      },

      active: {
        description: 'Browser Active flag',
        example: true
      },

    },
  
    fn: async function (inputs, exits) {

      let browserObj = {
        name: inputs.name,
        identifier: inputs.identifier,
        active: inputs.active
      }

      let indexResult = await sails.hooks.escall.esRequest('index', 'browsers', 'browser', browserObj);

      return exits.success(indexResult);
  
    }

  };
  