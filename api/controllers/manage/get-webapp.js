module.exports = {


    friendlyName: 'Get Web App',
  
  
    description: 'Get Web App',
  
  
    inputs: {
  
      identifier: {
        description: 'Web App Identifier',
        example: 'youfit'
      },

    },
  
    fn: async function (inputs, exits) {

      let getResult = await sails.hooks.escall.esRequest('search', 'applications', 'application', {q:`identifier:${inputs.identifier}`});

      return exits.success(getResult.hits.hits[0]._source);
  
    }

  };
  