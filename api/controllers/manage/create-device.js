module.exports = {


    friendlyName: 'Create Device',
  
  
    description: 'Create Device',
  
  
    inputs: {
  
      name: {
        description: 'test device',
        example: 'iPhone X'
      },

      identifier: {
        description: 'test device identifier',
        example: 'iphonex'
      },

      width: {
        description: 'test device viewport width',
        example: '1920'
      },

      height: {
        description: 'test device viewport height',
        example: '1080'
      },

      viewport: {
        description: 'test device viewport',
        example: '1920x1080'
      },

      active: {
        description: 'Device Active flag',
        example: true
      },

    },
  
    fn: async function (inputs, exits) {

      let deviceObj = {
        name: inputs.name,
        identifier: inputs.identifier,
        width: parseInt(inputs.width),
        height: parseInt(inputs.height),
        viewport: inputs.viewport,
        active: inputs.active
      }

      let indexResult = await sails.hooks.escall.esRequest('index', 'devices', 'device', deviceObj);

      return exits.success(indexResult);
  
    }

  };
  