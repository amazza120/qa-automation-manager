module.exports = {


    friendlyName: 'Update Web App',
  
  
    description: 'Update Web App',
  
  
    inputs: {
  
      name: {
        description: 'Web App',
        example: 'Youfit'
      },

      identifier: {
        description: 'Web App Identifier',
        example: 'youfit'
      },

      defaultUrl: {
        description: 'Web App Default URL',
        example: 'https://youfit.com'
      },

      active: {
        description: 'Web App Active flag',
        example: true
      },

      projectId: {
        description: 'Cypress Project Id',
        example: 'yri1wi'
      },

      cypressKey: {
        description: 'Cypress Key',
        example: '40a170d0-6533-4ae7-bcb1-71f240f9d985'
      },

    },
  
    fn: async function (inputs, exits) {

      let getResult = await sails.hooks.escall.esRequest('search', 'applications', 'application', {q:`identifier:${inputs.identifier}`});
      let updateResult = await sails.hooks.escall.esRequest('update', 'applications', 'application', {
        id: getResult.hits.hits[0]._id,
        body: {
          doc: {
            name: inputs.name,
            identifier: inputs.identifier,
            default_url: inputs.defaultUrl,
            active: inputs.active,
            project_id: inputs.projectId,
            cypress_key: inputs.cypressKey
          }
        }
      });

      return exits.success(updateResult);
  
    }

  };
  