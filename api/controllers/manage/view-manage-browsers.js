module.exports = {


  friendlyName: 'View manage browsers dashboard',


  description: 'Display "Manage Browsers Dashboard" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/manage/manage-browsers',
    }

  },


  fn: async function (inputs, exits) {

    let browsers = await sails.hooks.escall.esRequest('search', 'browsers', 'browser', {q:'*'}, {s:'identifier:desc'});

    return exits.success({browsers: browsers.hits.hits});

  }


};
