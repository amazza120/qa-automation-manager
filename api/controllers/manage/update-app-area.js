module.exports = {


    friendlyName: 'Update Application Area',
  
  
    description: 'Update Application Area',
  
  
    inputs: {
  
      name: {
        description: 'application area display name',
        example: 'Product Detail Page'
      },

      identifier: {
        description: 'application area identifier',
        example: 'pdp'
      },

      app_identifier: {
        description: 'application identifier',
        example: 'youfit'
      },

      active: {
        description: 'application area active flag',
        example: true
      },

    },
  
    fn: async function (inputs, exits) {

      let getResult = await sails.hooks.escall.esRequest('search', 'app_areas', 'area', {q:`identifier:${inputs.identifier} AND app_identifier:${inputs.app_identifier}`});
      let updateResult = await sails.hooks.escall.esRequest('update', 'app_areas', 'area', {
        id: getResult.hits.hits[0]._id,
        body: {
          doc: {
            name: inputs.name,
            identifier: inputs.identifier,
            app_identifier: inputs.app_identifier,
            active: inputs.active
          }
        }
      });

      return exits.success(updateResult);
  
    }

  };
  