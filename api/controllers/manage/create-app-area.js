module.exports = {


    friendlyName: 'Create Application Area',
  
  
    description: 'Create Application Area',
  
  
    inputs: {
  
      name: {
        description: 'application area display name',
        example: 'Product Detail Page'
      },

      identifier: {
        description: 'application area identifier',
        example: 'pdp'
      },

      app_identifier: {
        description: 'application identifier',
        example: 'youfit'
      },

      active: {
        description: 'application area active flag',
        example: true
      },

    },
  
    fn: async function (inputs, exits) {

      let appAreaObj = {
        name: inputs.name,
        identifier: inputs.identifier,
        app_identifier: inputs.app_identifier,
        active: inputs.active
      }

      let indexResult = await sails.hooks.escall.esRequest('index', 'app_areas', 'area', appAreaObj);

      return exits.success(indexResult);
  
    }

  };
  