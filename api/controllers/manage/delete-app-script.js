module.exports = {


    friendlyName: 'Delete Script',
  
  
    description: 'Delete Script',
  
  
    inputs: {

      file_name: {
        description: 'script file name',
        example: 'pdp-load-time.spec.js'
      },

      app_identifier: {
        description: 'application identifier',
        example: 'youfit'
      },

      app_area_identifier: {
        description: 'application area identifier',
        example: 'pdp'
      },

    },
  
    fn: async function (inputs, exits) {

      let getResult = await sails.hooks.escall.esRequest('search', 'app_scripts', 'script', {q:`file_name:${inputs.file_name} AND app_identifier:${inputs.app_identifier} AND app_area_identifier:${inputs.app_area_identifier}`});
      let deleteResult = await sails.hooks.escall.esRequest('delete', 'app_scripts', 'script', {id: getResult.hits.hits[0]._id});

      return exits.success(deleteResult);
  
    }

  };
  