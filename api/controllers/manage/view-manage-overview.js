module.exports = {


  friendlyName: 'View manage dashboard overview',


  description: 'Display "Manage Dashboard Overview" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/manage/manage-overview',
    }

  },


  fn: async function (inputs, exits) {

    return exits.success();

  }


};
