module.exports = {


  friendlyName: 'View manage devices dashboard',


  description: 'Display "Manage Devices Dashboard" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/manage/manage-devices',
    }

  },


  fn: async function (inputs, exits) {

    let testDevices = await sails.hooks.escall.esRequest('search', 'devices', 'device', {q:'*'}, {s:'identifier:desc'});

    return exits.success({devices: testDevices.hits.hits});

  }


};
