module.exports = {


  friendlyName: 'Get Jira Issue',


  description: 'Get Jira Issue',


  inputs: {

    jiraIssueKey: {
      description: 'Jira Key',
      example: 'ASUM-378'
    },
    
  },

  fn: async function (inputs, exits) {

    // check if the jira ticket exists. the data gethering will be done on the server side.
    let getResult = await sails.hooks.jiracall.jiraRequest('get', `/rest/api/2/issue/${inputs.jiraIssueKey}`, { fields: 'id' });

    return exits.success(getResult.data ? true : false);

  }

};
