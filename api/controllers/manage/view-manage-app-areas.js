module.exports = {


  friendlyName: 'View Manage Application Areas Dashboard',


  description: 'Display "Manage Application Areas Dashboard" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/manage/manage-app-areas',
    }

  },


  fn: async function (inputs, exits) {

    let apps = await await sails.hooks.escall.esRequest('search', 'applications', 'application', {q:'*'}, {s:'identifier:desc'});

    return exits.success({apps: apps.hits.hits});

  }


};
