module.exports = {


  friendlyName: 'Get Device',


  description: 'Get Device',


  inputs: {

    identifier: {
      description: 'test device identifier',
      example: 'iphonex'
    },
    
  },

  fn: async function (inputs, exits) {

    let getResult = await sails.hooks.escall.esRequest('search', 'devices', 'device', {q:`identifier:${inputs.identifier}`});

    return exits.success(getResult.hits.hits[0]._source);

  }

};
