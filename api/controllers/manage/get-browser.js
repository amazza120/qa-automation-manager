module.exports = {


  friendlyName: 'Get Browser',


  description: 'Get Browser',


  inputs: {

    identifier: {
      description: 'test browser identifier',
      example: 'chrome'
    },
    
  },

  fn: async function (inputs, exits) {

    let getResult = await sails.hooks.escall.esRequest('search', 'browsers', 'browser', {q:`identifier:${inputs.identifier}`});

    return exits.success(getResult.hits.hits[0]._source);

  }

};
