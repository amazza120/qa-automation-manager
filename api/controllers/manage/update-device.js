module.exports = {


    friendlyName: 'Update Device',
  
  
    description: 'Update Device',
  
  
    inputs: {
  
      name: {
        description: 'test device',
        example: 'iPhone X'
      },

      identifier: {
        description: 'test device identifier',
        example: 'iphonex'
      },

      width: {
        description: 'test device viewport width',
        example: '1920'
      },

      height: {
        description: 'test device viewport height',
        example: '1080'
      },

      viewport: {
        description: 'test device viewport',
        example: '1920x1080'
      },

      active: {
        description: 'Device Active flag',
        example: true
      },

    },
  
    fn: async function (inputs, exits) {

      let getResult = await sails.hooks.escall.esRequest('search', 'devices', 'device', {q:`identifier:${inputs.identifier}`});
      let updateResult = await sails.hooks.escall.esRequest('update', 'devices', 'device', {
        id: getResult.hits.hits[0]._id,
        body: {
          doc: {
            name: inputs.name,
            identifier: inputs.identifier,
            width: parseInt(inputs.width),
            height: parseInt(inputs.height),
            viewport: inputs.viewport,
            active: inputs.active
          }
        }
      });

      return exits.success(updateResult);
  
    }

  };
  