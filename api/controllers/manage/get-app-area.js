module.exports = {


  friendlyName: 'Get Application Area',


  description: 'Get Application Area',


  inputs: {

    identifier: {
      description: 'application area identifier',
      example: 'pdp'
    },

    app_identifier: {
      description: 'application identifier',
      example: 'youfit'
    },

    active: {
      description: 'application area active flag',
      example: true
    },
    
  },

  fn: async function (inputs, exits) {

    let getResult = '';

    if (inputs.active) {
      getResult = await sails.hooks.escall.esRequest('search', 'app_areas', 'area', {q:`app_identifier:${inputs.app_identifier} AND active:${inputs.active}`});
    }
    else {
      // if all required query elements exist (app and app area); query for the exact document. If not, get all for that specified app
      getResult = inputs.identifier && inputs.app_identifier ? 
        await sails.hooks.escall.esRequest('search', 'app_areas', 'area', {q:`identifier:${inputs.identifier} AND app_identifier:${inputs.app_identifier}`}) :
        await sails.hooks.escall.esRequest('search', 'app_areas', 'area', {q:`app_identifier:${inputs.app_identifier}`});
    }

    if (inputs.identifier && inputs.app_identifier) { // get the specific document
      return exits.success(getResult.hits.hits[0]._source);
    }
    else { // get all documents associated
      return exits.success(getResult.hits.hits);
    }
  }

};
