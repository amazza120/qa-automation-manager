module.exports = {


  friendlyName: 'Get Script',


  description: 'Get Script',


  inputs: {

    file_name: {
      description: 'script file name',
      example: 'pdp-load-time.spec.js'
    },

    app_identifier: {
      description: 'application identifier',
      example: 'youfit'
    },

    app_area_identifier: {
      description: 'application area identifier',
      example: 'pdp'
    },

    active: {
      description: 'application area active flag',
      example: true
    },
    
  },

  fn: async function (inputs, exits) {

    let getResult = '';

    if (inputs.active) {
      getResult = await sails.hooks.escall.esRequest('search', 'app_scripts', 'script', {q:`app_identifier:${inputs.app_identifier} AND app_area_identifier:${inputs.app_area_identifier} AND active:${inputs.active}`});
    }
    else {
      // if all required query elements exist (file name, app area, app); query for the exact document. If not, get all for that specified app area
      getResult = inputs.file_name && inputs.app_identifier && inputs.app_area_identifier ? 
        await sails.hooks.escall.esRequest('search', 'app_scripts', 'script', {q:`file_name:${inputs.file_name} AND app_identifier:${inputs.app_identifier} AND app_area_identifier:${inputs.app_area_identifier}`}) :
        await sails.hooks.escall.esRequest('search', 'app_scripts', 'script', {q:`app_identifier:${inputs.app_identifier} AND app_area_identifier:${inputs.app_area_identifier}`});
    }

    if (inputs.file_name && inputs.app_identifier && inputs.app_area_identifier) { // get the specific document
      return exits.success(getResult.hits.hits[0]._source);
    }
    else { // get all documents associated
      return exits.success(getResult.hits.hits);
    }
  }

};
