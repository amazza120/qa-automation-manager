module.exports = {


  friendlyName: 'View manage web applications dashboard',


  description: 'Display "Manage Web Applications Dashboard" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/manage/manage-webapps',
    }

  },


  fn: async function (inputs, exits) {

    let testApps = await sails.hooks.escall.esRequest('search', 'applications', 'application', {q:'*'}, {s:'identifier:desc'});

    return exits.success({applications: testApps.hits.hits});

  }


};
