module.exports = {


    friendlyName: 'Delete Device',
  
  
    description: 'Delete Device',
  
  
    inputs: {
      
      identifier: {
        description: 'Device Identifier',
        example: 'iphonex'
      },

    },
  
    fn: async function (inputs, exits) {

      let getResult = await sails.hooks.escall.esRequest('search', 'devices', 'device', {q:`identifier:${inputs.identifier}`});
      let deleteResult = await sails.hooks.escall.esRequest('delete', 'devices', 'device', {id: getResult.hits.hits[0]._id});

      return exits.success(deleteResult);
  
    }

  };
  