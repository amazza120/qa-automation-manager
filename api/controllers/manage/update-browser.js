module.exports = {


    friendlyName: 'Update Browser',
  
  
    description: 'Update Browser',
  
  
    inputs: {
  
      name: {
        description: 'test browser',
        example: 'Chrome'
      },

      identifier: {
        description: 'test browser identifier',
        example: 'chrome'
      },

      active: {
        description: 'Chrome Active flag',
        example: true
      },

    },
  
    fn: async function (inputs, exits) {

      let getResult = await sails.hooks.escall.esRequest('search', 'browsers', 'browser', {q:`identifier:${inputs.identifier}`});
      let updateResult = await sails.hooks.escall.esRequest('update', 'browsers', 'browser', {
        id: getResult.hits.hits[0]._id,
        body: {
          doc: {
            name: inputs.name,
            identifier: inputs.identifier,
            active: inputs.active
          }
        }
      });

      return exits.success(updateResult);
  
    }

  };
  