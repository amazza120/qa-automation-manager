module.exports = {


    friendlyName: 'Delete Browser',
  
  
    description: 'Delete Browser',
  
  
    inputs: {
      
      identifier: {
        description: 'Browser Identifier',
        example: 'chrome'
      },

    },
  
    fn: async function (inputs, exits) {

      let getResult = await sails.hooks.escall.esRequest('search', 'browsers', 'browser', {q:`identifier:${inputs.identifier}`});
      let deleteResult = await sails.hooks.escall.esRequest('delete', 'browsers', 'browser', {id: getResult.hits.hits[0]._id});

      return exits.success(deleteResult);
  
    }

  };
  