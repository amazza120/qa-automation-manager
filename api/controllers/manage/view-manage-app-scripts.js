module.exports = {


  friendlyName: 'View Manage Test Scripts Dashboard',


  description: 'Display "Manage Test Scripts Dashboard" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/manage/manage-app-scripts',
    }

  },


  fn: async function (inputs, exits) {

    let apps = await await sails.hooks.escall.esRequest('search', 'applications', 'application', {q:'*'}, {s:'identifier:desc'});

    return exits.success({apps: apps.hits.hits});

  }


};
