module.exports = {


    friendlyName: 'Delete Web App',
  
  
    description: 'Delete Web App',
  
  
    inputs: {
      
      identifier: {
        description: 'Web App Identifier',
        example: 'youfit'
      },

    },
  
    fn: async function (inputs, exits) {

      let getResult = await sails.hooks.escall.esRequest('search', 'applications', 'application', {q:`identifier:${inputs.identifier}`});
      let deleteResult = await sails.hooks.escall.esRequest('delete', 'applications', 'application', {id: getResult.hits.hits[0]._id});

      return exits.success(deleteResult);
  
    }

  };
  