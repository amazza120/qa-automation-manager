module.exports = {


    friendlyName: 'Create Application Script',
  
  
    description: 'Create Application Script',
  
  
    inputs: {
  
      name: {
        description: 'test script display name',
        example: 'Product Detail Page Regression'
      },

      description: {
        description: 'description of the test script',
        example: 'Product Detail Page Regression. The script validates the following: 1) ... 2) ...'
      },

      file_name: {
        description: 'script file name',
        example: 'pdp-load-time.spec.js'
      },

      app_identifier: {
        description: 'application identifier',
        example: 'youfit'
      },

      app_area_identifier: {
        description: 'application area identifier',
        example: 'pdp'
      },

      active: {
        description: 'application area active flag',
        example: true
      },

    },
  
    fn: async function (inputs, exits) {

      let appScriptObj = {
        name: inputs.name,
        description: inputs.description,
        file_name: inputs.file_name,
        app_identifier: inputs.app_identifier,
        app_area_identifier: inputs.app_area_identifier,
        active: inputs.active
      }

      let indexResult = await sails.hooks.escall.esRequest('index', 'app_scripts', 'script', appScriptObj);

      return exits.success(indexResult);
  
    }

  };
  