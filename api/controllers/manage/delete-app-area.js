module.exports = {


    friendlyName: 'Delete Application Area',
  
  
    description: 'Delete Application Area',
  
  
    inputs: {
      
      identifier: {
        description: 'application area identifier',
        example: 'pdp'
      },

      app_identifier: {
        description: 'application identifier',
        example: 'youfit'
      },

    },
  
    fn: async function (inputs, exits) {

      let getResult = await sails.hooks.escall.esRequest('search', 'app_areas', 'area', {q:`identifier:${inputs.identifier} AND app_identifier:${inputs.app_identifier}`});
      let deleteResult = await sails.hooks.escall.esRequest('delete', 'app_areas', 'area', {id: getResult.hits.hits[0]._id});

      return exits.success(deleteResult);
  
    }

  };
  