async function cypressTestFailed(runId) {
    let res = await sails.hooks.escall.esRequest('count', 'test_data', 'result', {q:`run_id:${runId} AND result:failed`});
    return res.count > 0;
}

async function getCypressUrl(runId, projectId, stdout) {
    const failed = await cypressTestFailed(runId);
    const regex = new RegExp(`https://dashboard\\.cypress\\.io/#/projects/${projectId}/runs/\\d+`, 'gm');

    let url = regex.exec(stdout)[0];

    // if the child process was successful, parse the stdout and get the cypress url
    if (!failed) {
        return {passed: true, url: url};
    }
    else {
        return {passed: false, url: url};
    }
}

module.exports = function cypress(sails) {

    return {

        getCypressUrl: async function(runId, projectId, stdout) {
            return await getCypressUrl(runId, projectId, stdout);
        }
        
    }

 };