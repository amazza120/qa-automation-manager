async function getReportPath(stdout) {
    let regex = /\/reports\/64labs-test-report_(.*?).html/g;
    let repPath = stdout.match(regex);
    return {report: repPath};
}

module.exports = function reporter(sails) {

    return {

        getReportPath: async function(stdout) {
            return await getReportPath(stdout);
        }
        
    }

 };