const {OAuth2Client} = require('google-auth-library');
const {google} = require('googleapis');
const atob = require('atob');
const fs = require('fs');

const redirect_uris = sails.config.sockets.onlyAllowOrigins[0];
const {client_id, client_secret} = sails.config.gapi;

let gmail = google.gmail({
  version: 'v1'
});

module.exports = function gapiCall(sails) {

    return {

      gapiRequest: async (endpoint, query, fields, data, maxResults) => {
        switch(endpoint) {
          case 'client':

            console.log('gapi/index -> trigger client');

            const oAuth2Client = new OAuth2Client(
              client_id,
              client_secret,
              redirect_uris
            );

            const getTokenResp = await oAuth2Client.getToken(data.gAuthCode);
            oAuth2Client.setCredentials(getTokenResp.tokens);
            
            // setup google api settings
            gmail = google.gmail({
              version: 'v1',
              auth: oAuth2Client,
            });

            return 'success';
          case 'list':
            const listRes = await gmail.users.messages.list({
              userId: 'me',
              q: query,
              fields: fields,
              maxResults: maxResults
            });
            return listRes.data;
          case 'get':
            const getRes = await gmail.users.messages.get({
              userId: 'me',
              id: data.id
            });
            return getRes.data;
          case 'attachments':
            const attachRes = await gmail.users.messages.attachments.get({
              userId: 'me',
              messageId: data.messageId,
              id: data.attachmentId
            });

            // process pdf data using atob, ArrayBuffer, Uint8Array and fs.writeFile
            var binary = atob(attachRes.data.data);
            var len = binary.length;
            var arrBuffer = new ArrayBuffer(len);

            var view = new Uint8Array(arrBuffer);
            for (var i = 0; i < len; i++) {
                view[i] = binary.charCodeAt(i);
            }

            let dataBuffer = Buffer.from(arrBuffer);

            const resData = new Uint8Array(Buffer.from(dataBuffer));
            fs.writeFile(`${__dirname}/../../../assets/images/reports/pandora/test.pdf`, resData, (err) => {
              if (err) {
                throw err;
              }
              console.log('Latest Canvas file saved succesfully!');
            });

            return 'success';
        }     
      }
    }
 };