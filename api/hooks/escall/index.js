const elasticsearch = require('elasticsearch');

function getEsClient() {
    return new elasticsearch.Client({
        host: "https://apiuser:350764labs**@0a37b532368b4baab896f8b942009154.us-east-1.aws.found.io:9243",
        log: 'trace'
    });
}

module.exports = function esCall(sails) {

    return {

        // search documents - Query in the Lucene query string syntax
        esRequest: async function (method, index, type, data, sort) {
            let client = getEsClient();
            sort = sort ? sort : '';
            switch(method) {
                case "search":
                    try {
                        return await client.search({
                            index: index,
                            type: type,
                            q: data.q,
                            sort: sort.s,
                            size: 10000
                        });
                    } catch (error) {
                        console.error(error);
                    }
                case "index":
                    try {
                        return await client.index({
                            index: index,
                            type: type,
                            body: data
                        });
                    } catch (error) {
                        console.error(error);
                    }
                case "update":
                    try {
                        return await client.update({
                            index: index,
                            type: type,
                            id: data.id,
                            body: data.body
                        });
                    } catch (error) {
                        console.error(error);
                    }
                case "delete":
                    try {
                        return await client.delete({
                            index: index,
                            type: type,
                            id: data.id
                        });
                    } catch (error) {
                        console.error(error);
                    }
                case "count":
                    try {
                        return await client.count({
                            index: index,
                            type: type,
                            q: data.q
                        });
                    } catch (error) {
                        console.error(error);
                    }
            }
            
        }
        
    }
    
 };