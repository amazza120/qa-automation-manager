const axios = require('axios');

const {host, username, password} = sails.config.jira;

module.exports = function jiraCall(sails) {

    return {

        jiraRequest: async (method, endpoint, obj) => {
          if (method === 'get' && obj.fields) {
            try {
              return await axios({
                method: method,
                url: `${host}${endpoint}`,
                auth: {
                  username: username,
                  password: password
                },
                params: {
                  fields: obj.fields
                }
              })
            } catch (error) {
              console.error(error);
              return {result: 'error', error: error};
            }
          }
          else if (method === 'post' && obj.data) {
            try {
              return await axios({
                method: method,
                url: `${host}${endpoint}`,
                auth: {
                  username: username,
                  password: password
                },
                data: obj.data
              })
            } catch (error) {
              console.error(error);
              return {result: 'error', error: error};
            }
          }
        }
    }
    
 };