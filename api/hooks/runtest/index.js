const util = require('util');
const uuid = require('uuid/v1');
const execFile = util.promisify(require('child_process').execFile);

function getPath(identifier, testApp, appArea, spec) {
    // first part of the path, the application
    const app = identifier == 'testing' ? `${testApp}/` : '';

    // second, the application area
    const area = appArea ? `${appArea}` : '**';

    // last, if there is a specific file
    // note: deactivating the script or area on elasticsearch only prevents the user from selecting it
    // if you wish to deactivate the script completely, rename the extension to ".js.skip"
    const file = spec ? `${spec}` : '*.js';

    return `${app}${area}/${file}`;
}

async function execTestRun(options) {
    const runId = uuid();
    const username = options.user.toLowerCase().replace(/\s+/g, '-');
    const appLowerCase = options.application.name.toLowerCase().replace(/\s+/g, '-');
    const appIdentifier = options.application.identifier;
    const buildId = `${username}-${appLowerCase}-${runId}`;
    const jiraIssueKey = options.jiraIssueKey ? options.jiraIssueKey : 'optout';
    const specPath = getPath(options.application.identifier, options.application.testApp, options.appArea, options.spec);

    const {stdout} = await execFile('node', [
        `../cypress-${appIdentifier}/node_modules/.bin/cypress`,                                // file
        'run',                                                                                  // command
        '--browser', options.browser,                                                           // browser
        '--project', `../cypress-${appIdentifier}`,                                             // project
        '--record',                                                                             // record
        '--key', options.application.cypressKey,                                                // api key
        '--spec', `../cypress-${appIdentifier}/cypress/integration/${specPath}`,                // spec - ADD APPLICATION AREA OPTION `../cypress-testing/cypress/integration/${appArea}/**/*`
        '--group', buildId,                                                                     // group
        '--ci-build-id', buildId,                                                               // build id - https://github.com/cypress-io/cypress/issues/2346
        '--env', `runid=${buildId},device=${options.device.name},width=${options.device.width},height=${options.device.height},application=${appIdentifier},testurl=${options.testUrl},username=${username},jiraissuekey=${jiraIssueKey}` // environment variables
    ]).catch(error => {
        console.log(`execFile error - ${error.message}`);
        return {stdout: error.stdout};
    });

    console.log(`execFile stdout - Env: device=${options.device.name},width=${options.device.width},height=${options.device.height},application=${appIdentifier},testurl=${options.testUrl},username=${username},jiraissuekey=${jiraIssueKey} | Result: ${stdout}`);
    return {uuid: buildId, stdout: stdout};
}

module.exports = function runTest(sails) {

    return {

        runTest: async function(options) {
            return await execTestRun(options);
        }
        
    }

 };