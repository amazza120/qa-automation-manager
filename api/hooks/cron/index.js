let cron = require('node-cron');

module.exports = function cronCall(sails) {

    return {

      startRequest: async (code) => {

        await sails.hooks.gapi.gapiRequest('client', '', '', {gAuthCode: code}, '');

        // every 5 minutes
        cron.schedule('*/5 * * * *', async () => {

          // get a list of canvas report messages within the past day and limit it to the latest result
          const latestMessage = await sails.hooks.gapi.gapiRequest('list', 'from:noreply@watcheralert.found.io subject:Test Canvas Report - Beta newer_than:1d', 'messages/id', {}, 1);

          // console.log(`cron/index -> latestMessage: ${latestMessage.messages[0].id}`);

          // get attachment id on the latest canvas report message
          const messageDetails = await sails.hooks.gapi.gapiRequest('get', '', '', {id: latestMessage.messages[0].id}, '');

          // console.log(`cron/index -> messageDetails: ${messageDetails.payload.parts[1].body.attachmentId}`);

          // get the message attachement
          const attachmentData = await sails.hooks.gapi.gapiRequest('attachments', '', '', {messageId: latestMessage.messages[0].id, attachmentId: messageDetails.payload.parts[1].body.attachmentId}, '');

          console.log(`cron/index -> attachmentData: ${attachmentData}`);

          return attachmentData;

        });
      }
    }
 };