module.exports = function (req, res, next) {

    // Find an access header
    var accessToken = req.header('64-api-key');

    // No header, no access
    if (!accessToken) {
        return res.forbidden();
    }

    if (accessToken === sails.config.custom.apiSecret) {
        return next();
    }
    else {
        return res.forbidden();
    }
 
 }