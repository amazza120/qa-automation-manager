# qa-automation-manager

## Description

The QA Automation Manager serves as a frontend controller to 64labs QA Automation testing framework [Cypress.io](https://www.cypress.io).
The tool also serves as a administrative tool to the data layer [Elastic.co](https://www.elastic.co).

## QA Automation Infrastructure

1. The Frontend - QA Automation Manager - Sends user inputs to the backend infrastructure [Cypress.io](https://www.cypress.io) and [Elastic.co](https://www.elastic.co)
2. [Cypress.io](https://www.cypress.io) - Test Runner and Testing Framework
3. [Elastic.co](https://www.elastic.co) - Elasticsearch data layer and Kibana for visiualization

## Setup

Please the *Sails* section for local setup

#SAILS

a [Sails v1](https://sailsjs.com) application


### Links

+ [Get started](https://sailsjs.com/get-started)
+ [Sails framework documentation](https://sailsjs.com/documentation)
+ [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)
+ [Professional / enterprise options](https://sailsjs.com/enterprise)

