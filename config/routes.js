/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝

  'GET /':                          { action: 'view-homepage-or-redirect' },
  'GET /welcome':                   { action: 'dashboard/view-welcome' },
  
  'GET /faq':                       { view:   'pages/faq' },
  'GET /legal/terms':               { view:   'pages/legal/terms' },
  'GET /legal/privacy':             { view:   'pages/legal/privacy' },
  'GET /contact':                   { view:   'pages/contact' },

  'GET /logout':                    { action: 'account/logout' },

  'GET /manage':                    { action: 'manage/view-manage-overview' },
  'GET /manage/devices':            { action: 'manage/view-manage-devices' },
  'GET /manage/webapps':            { action: 'manage/view-manage-webapps' },
  'GET /manage/browsers':           { action: 'manage/view-manage-browsers' },
  'GET /manage/app-areas':          { action: 'manage/view-manage-app-areas' },
  'GET /manage/app-scripts':        { action: 'manage/view-manage-app-scripts' },

  'GET /kibana':                    { action: 'kibana/view-kibana-projects' },

  'GET /kibana/general':            { action: 'kibana/dashboard/view-general-dashboard' },
  'GET /kibana/ann-summers':        { action: 'kibana/dashboard/view-ann-summers-dashboard' },
  'GET /kibana/carparts':           { action: 'kibana/dashboard/view-carparts-dashboard' },
  'GET /kibana/kirklands':          { action: 'kibana/dashboard/view-kirklands-dashboard' },
  'GET /kibana/mackage':            { action: 'kibana/dashboard/view-mackage-dashboard' },
  'GET /kibana/soia-kyo':           { action: 'kibana/dashboard/view-soia-kyo-dashboard' },
  'GET /kibana/youfit':             { action: 'kibana/dashboard/view-youfit-dashboard' },

  'GET /kibana/pandora':            { action: 'kibana/view-pandora-menu' },
  'GET /kibana/pandora-canvas':     { action: 'kibana/canvas/view-pandora-canvas' },
  'GET /kibana/pandora-dashboard':  { action: 'kibana/dashboard/view-pandora-dashboard' },

  'GET /sails-auth':                { action: 'entrance/sails-auth' },

  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝
  // Note that, in this app, these API endpoints may be accessed using the `Cloud.*()` methods
  // from the CloudSDK library.

  // Custom API Endpoints
  'POST /run-test':           { action: 'dashboard/run-test' },

  'GET /get-device':          { action: 'manage/get-device' },
  'POST /create-device':      { action: 'manage/create-device' },
  'POST /update-device':      { action: 'manage/update-device' },
  'POST /delete-device':      { action: 'manage/delete-device' },

  'GET /get-webapp':          { action: 'manage/get-webapp' },
  'POST /create-webapp':      { action: 'manage/create-webapp' },
  'POST /update-webapp':      { action: 'manage/update-webapp' },
  'POST /delete-webapp':      { action: 'manage/delete-webapp' },

  'GET /get-browser':         { action: 'manage/get-browser' },
  'POST /create-browser':     { action: 'manage/create-browser' },
  'POST /update-browser':     { action: 'manage/update-browser' },
  'POST /delete-browser':     { action: 'manage/delete-browser' },

  'GET /get-app-area':        { action: 'manage/get-app-area' },
  'POST /create-app-area':    { action: 'manage/create-app-area' },
  'POST /update-app-area':    { action: 'manage/update-app-area' },
  'POST /delete-app-area':    { action: 'manage/delete-app-area' },

  'GET /get-app-script':      { action: 'manage/get-app-script' },
  'POST /create-app-script':  { action: 'manage/create-app-script' },
  'POST /update-app-script':  { action: 'manage/update-app-script' },
  'POST /delete-app-script':  { action: 'manage/delete-app-script' },

  'GET /get-jira-issue':      { action: 'manage/get-jira-issue' },

  'GET /get-kibana-canvas':   { action: 'kibana/get-kibana-canvas' },
  'GET /start-cron-job':      { action: 'kibana/start-cron-job' },

  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝

  // routers here

  //  ╔╦╗╦╔═╗╔═╗  ╦═╗╔═╗╔╦╗╦╦═╗╔═╗╔═╗╔╦╗╔═╗
  //  ║║║║╚═╗║    ╠╦╝║╣  ║║║╠╦╝║╣ ║   ║ ╚═╗
  //  ╩ ╩╩╚═╝╚═╝  ╩╚═╚═╝═╩╝╩╩╚═╚═╝╚═╝ ╩ ╚═╝

  '/terms':                   '/legal/terms',
  '/oauth2callback':          '/'

};
